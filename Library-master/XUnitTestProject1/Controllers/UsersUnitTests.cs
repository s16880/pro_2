﻿using Library.Controllers;
using Library.Entities;
using Library.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestProject1.Controllers
{
    public class UsersUnitTests
    {
        [Fact]
        public async Task GetUsers_200Ok()
        {
            //arrange
            var m = new Mock<IUserRepository>();

            ICollection<User> users = new List<User>
            {
                new User{IdUser=1, Name="kowalski", Email="kowalski@gmail.com"},
                new User{IdUser=2, Name="nowacki", Email="nowacki@gmail.com"},
                new User{IdUser=3, Name="nowak", Email="nowak@gmail.com"}
            };

            m.Setup(c => c.GetUsers()).Returns(Task.FromResult(users));
            var controller = new UsersController(m.Object);

            //act
            var result = await controller.GetUsers();

            //assert
            Assert.True(result is OkObjectResult);
            var r = result as OkObjectResult;
            Assert.True((r.Value as ICollection<User>).Count == 3);
            Assert.True((r.Value as ICollection<User>).ElementAt(0).Name == "kowalski");
        }
        [Fact]
        public async Task GetUser_200Ok()
        {
            //arrange
            var m = new Mock<IUserRepository>();

            ICollection<User> users = new List<User>
            {
                new User{IdUser=1, Name="kowalski", Email="kowalski@gmail.com"},
                new User{IdUser=2, Name="nowacki", Email="nowacki@gmail.com"}
            };

            m.Setup(c => c.GetUser(2)).Returns(Task.FromResult(users.First(u => u.IdUser == 2)));
            var controller = new UsersController(m.Object);

            //act
            var result = await controller.GetUser(2);

            //assert
            Assert.True(result is OkObjectResult);
            var r = result as OkObjectResult;
            Assert.True((r.Value as User).Name == "nowacki");
        }
    }
}
