﻿using Library.Entities;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestProject1.Controllers
{
    public class BookBorrowsInitTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public BookBorrowsInitTests()
        {
            _server = ServerFactory.GetServerInstance();
            _client = _server.CreateClient();

            using (var scope = _server.Host.Services.CreateScope())
            {
                var _db = scope.ServiceProvider.GetRequiredService<LibraryContext>();

                //_db.BookBorrow.Add(new BookBorrow
                //{
                //    IdBookBorrow = 1,
                //    IdUser = 1,
                //    IdBook = 12,
                //    BorrowDate = new DateTime(2020, 03, 15),
                //    Comments = "no comment"
                //});

                //_db.SaveChanges();

            }
        }

        [Fact]
        public async Task AddBookBorrow_201Created()
        {
            var newBook = new BookBorrow{
                IdBookBorrow = 4,
                IdUser = 2,
                IdBook = 2,
                BorrowDate = new DateTime(2020, 03, 16),
                Comments = "no comment"
            };
            
            StringContent contentSent = new StringContent(JsonConvert.SerializeObject(newBook), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync($"{_client.BaseAddress.AbsoluteUri}api/book-borrows", contentSent);
            httpResponse.EnsureSuccessStatusCode();
            var content = await httpResponse.Content.ReadAsStringAsync();
            var addedBookBorrow = JsonConvert.DeserializeObject<BookBorrow>(content);

            Assert.True(addedBookBorrow.IdBook==2);
        }
      
    }
}
