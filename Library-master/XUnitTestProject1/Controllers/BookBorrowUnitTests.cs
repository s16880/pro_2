﻿using Library.Controllers;
using Library.Entities;
using Library.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestProject1.Controllers
{
    public class BookBorrowUnitTests
    {
        
        [Fact]
        public async Task AddBookBorrow_201CreatedAtRoute()
        {
            var m = new Mock<IBookBorrowRepository>();
            ICollection<BookBorrow> bookBorrows = new List<BookBorrow>();

            BookBorrow bb = new BookBorrow { IdBookBorrow = 2, IdUser = 4, IdBook = 5, BorrowDate = DateTime.Now };

            //m.Setup(c => c.AddBookBorrow(bb)).returns(Task.FromResult(bookBorrows));
            //var controller = new BookBorrowsController(m.Object);

            //var result = await controller.AddBookBorrow(bb);

            Assert.True(result is OkObjectResult);
            var r = result as OkObjectResult;
            Assert.True((r.Value as ICollection<BookBorrow>).Count == 1);

        }
    }
}
